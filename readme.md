helloXPages
===========

This is a sample IBM Notes/Domino Application that is managed by Git. It is for testing purposes only.

Author
------

Grant Gingell, Archon Development Corp.

Installation
------------

This guide can be used for cloning any existing repo into a Notes/Domino application.

0. Create a new project in Designer.
 
    File > Application > New...

1. Clone the 'helloXPages' repo into your local Domino Designer workspace directory.

For example, clone here:

    C:\Lotus\NotesData\workspace

So that the helloXPages' files end up in this folder:

    C:\Lotus\NotesData\workspace\helloXPages

2. In Designer open the Package Explorer perspective. 

    Window > Show Eclipse Views > Package Explorer

3. Right click on the project created in step 0 in the Package Explorer and select "Import...".

4. In the "Import" dialog, open "General" and select "Existing Projects into Workspace"

5. In "Select root directory" choose the newly cloned project folder created in step 1.

6. Click "Finish"

7. In the Databse Navigator right click on the project created in step 0. Select:
    
    Team Development > Associate with Existing On-Disk Project

8. Select the folder imported in steps 3-6. 

9. Click "Finish"

Notes
-----

    * Your cloned project files will now be in the newly created application database. It is at this point you may want to install the Git plug-in for Designer.

    * In order to import a cloned project into DDE you must either create an Eclipse project with the files - or - make sure your repo containes the original Eclipse .project file in the project's root directory.


